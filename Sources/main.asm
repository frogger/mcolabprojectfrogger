;*****************************************************************
;* This stationery serves as the framework for a                 *
;* user application (single file, absolute assembly application) *
;* For a more comprehensive program that                         *
;* demonstrates the more advanced functionality of this          *
;* processor, please see the demonstration applications          *
;* located in the examples subdirectory of the                   *
;* Freescale CodeWarrior for the HC12 Program directory          *
;*****************************************************************

; export symbols
            XDEF Entry, _Startup            ; export 'Entry' symbol
            ABSENTRY Entry        ; for absolute assembly: mark this as application entry point



; Include derivative-specific definitions 
		INCLUDE 'derivative.inc' 

ROMStart    EQU  $4000  ; absolute address to place my code/constant data

; variable/data section

            ORG RAMStart
 ; Insert here your data definition.
Counter     DS.W 1
FiboRes     DS.W 1


; code section
            ORG   ROMStart


Entry:
_Startup:
            LDS   #RAMEnd+1       ; initialize the stack pointer

            CLI                     ; enable interrupts
	          MOVB	#$00,SCI0BDH 
            MOVB	#$0D,SCI0BDL 
            LDAB	SCI0CR1
            ANDB	#$EF
            STAB	SCI0CR1
            LDAB	SCI0CR2
            ORAB	#$0C
            STAB	SCI0CR2
mainLoop:
            
            LDAA #$43
            BSR SendChar
            JSR Delay
            
            LDAA #$4F
            BSR SendChar
            JSR Delay
            
            LDAA #$4F
            BSR SendChar
            JSR Delay
            
            LDAA #$4C
            BSR SendChar
            JSR Delay
            
            
            JMP *
            
SendChar:   
            PSHB
            LDAB  SCI0SR1
            ANDB  #$80
            CMPB  #$80 
            BNE   SendChar
            STAA  SCI0DRL
            PULB
            RTS
           

Delay:      LDX #$3A98	 
Delay1:     DBNE X, Delay1

     	      RTS
            

;**************************************************************
;*                 Interrupt Vectors                          *
;**************************************************************
            ORG   $FFFE
            DC.W  Entry           ; Reset Vector
